.. You should enable this project on travis-ci.org and coveralls.io to make
   these badges work. The necessary Travis and Coverage config files have been
   generated for you.

.. image:: https://travis-ci.org/memaldi/ckanext-welive_authentication.svg?branch=master
    :target: https://travis-ci.org/memaldi/ckanext-welive_authentication

.. image:: https://coveralls.io/repos/memaldi/ckanext-welive_authentication/badge.png?branch=master
  :target: https://coveralls.io/r/memaldi/ckanext-welive_authentication?branch=master

=============
ckanext-welive_authentication
=============

This extension allows the authentication against WeLive's AAC service.

------------
Requirements
------------

* CKAN 2.4.1

------------------------
Development Installation
------------------------

To install ckanext-welive_authentication for development, activate your CKAN virtualenv and
do::

    git clone https://bitbucket.org/welive/ckanext-welive_authentication.git
    cd ckanext-welive_authentication
    python setup.py develop
    pip install -r dev-requirements.txt


---------------
Config Settings
---------------

In the configuration .ini file of CKAN, include the following configuration variables::

    [plugin:authentication]
    validation_url = <URL to AAC validation service>
    login_url = <URL to AAC login service>
