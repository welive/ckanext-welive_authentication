from pylons.controllers.util import redirect
from ckan.plugins import toolkit as tk
from ckanext.welive_authentication import plugin
from ckan.lib import helpers as h
from ckan.common import request
import ckan.lib.base as base
import ConfigParser
import os
import urlparse
from pylons import request

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:authentication'
LOGIN_URL = config.get(PLUGIN_SECTION, 'login_url')
CLIENT_ID = config.get(PLUGIN_SECTION, 'client_id')
AAC_API_URL = config.get(PLUGIN_SECTION, 'aac_api_url')
AAC_URL = config.get(PLUGIN_SECTION, 'aac_url')
WELIVE_URL = config.get(PLUGIN_SECTION, 'welive_url')

MAIN_SECTION = 'app:main'
CKAN_SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

c = tk.c
render = tk.render


class WeliveAuthenticationController(base.BaseController):
    def login(self):
        # return redirect('%s?service=%s' % (LOGIN_URL, CKAN_SITE_URL))
        callback = '{}/{}/'.format(CKAN_SITE_URL, h.lang())
        if 'came_from' in request.params:
            came_from = request.params.get('came_from')
            path = urlparse.urlparse(came_from).path
            ckan_url_parsed = urlparse.urlparse(CKAN_SITE_URL)
            root = '{}://{}'.format(ckan_url_parsed.scheme,
                                    ckan_url_parsed.netloc)
            # There is some problem when the URL has an slash and parameters
            # /?code=somecode is a wrong URL for CKAN
            callback = '{}{}'.format(root, path)
        return redirect(
            '%s/eauth/authorize?client_id=%s&response_type=code&'
            'redirect_uri=%s&'
            'scope=profile.basicprofile.me,profile.accountprofile.me,'
            'cdv.profile.me,profile.basicprofile.all' %
            (AAC_URL, CLIENT_ID, callback))

    def register(self):
        return redirect('%s/?p_p_id=58&p_p_lifecycle=0&'
                        'p_p_state=maximized&p_p_mode=view&'
                        'p_p_col_id=column-1&p_p_col_count=1&saveLastPath=0&'
                        '_58_struts_action=%2Flogin%2Fcreate_account' %
                        AAC_URL)

    def logout(self):
        if 'RelayState' in request.GET:
            plugin.remove_cookie()
            return redirect('%s/cas/logout?RelayState=%s' % (AAC_URL, request.GET.get('RelayState')))
        else:
            print '%s/cas/logout?service=%s' % (AAC_URL, WELIVE_URL)
            return redirect('%s/cas/logout?service=%s' % (AAC_URL, WELIVE_URL))