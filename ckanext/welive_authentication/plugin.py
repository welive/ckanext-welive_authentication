from ckan.common import request, _
from routes.mapper import SubMapper
from pylons import request as py_req
from pylons.controllers.util import redirect
from ckan.lib import helpers as h
from ckan.model.user import User
import ckan.lib.base as base
import ckan.logic as logic
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.model as model
import logging
import requests
import pylons
import ConfigParser
import os
import uuid
import datetime
import base64
import json

NotFound = logic.NotFound

abort = base.abort

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:authentication'
VALIDATION_URL = config.get(PLUGIN_SECTION, 'validation_url')
AAC_API_URL = config.get(PLUGIN_SECTION, 'aac_api_url')
CLIENT_ID = config.get(PLUGIN_SECTION, 'client_id')
CLIENT_SECRET = config.get(PLUGIN_SECTION, 'client_secret')
ADMIN_CC_USER_ID = config.get(PLUGIN_SECTION, 'admin_cc_user_id')

MAIN_SECTION = 'app:main'
CKAN_SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

WELIVE_SECTION = 'plugin:welive_utils'
BASIC_USER = config.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(WELIVE_SECTION, 'basic_password')


def get_user_by_id(user_id):
    user = User.get(str(user_id))
    if user is not None:
        return user

    for user in User.all():
        try:
            if user.name in ['default', 'visitor', 'logged_in']:
                return None
            elif int(user.name) == int(user_id):
                return user
        except Exception as e:  # pragma: no cover
            log.error(e)
    return None


def get_user_email(token):
    response = requests.get('%s/accountprofile/me' % AAC_API_URL,
                            headers={'Authorization':
                                     '%s' % token}).json()
    email = ''
    if 'accounts' in response:
        if 'welive' in response['accounts']:
            email = response['accounts']['welive']['username']
        elif 'google' in response['accounts']:
            email = response['accounts']['google']['OIDC_CLAIM_email']
        if 'facebook' in response['accounts']:
            email = response['accounts']['facebook']['email']
    return email


def get_user_id(token):
    response = requests.get('%s/basicprofile/me' % AAC_API_URL,
                            headers={'Authorization':
                                     '%s' % token}).json()
    return response.get('userId', None)


def create_user(cc_user_id):
    name = '0' + str(cc_user_id)
    fullname = 'WeLive User {}'.format(name)
    user = model.User(
            name=name,
            fullname=fullname,
            email='fake@welive.eu',
            password=str(uuid.uuid4())
        )
    user.save()
    model.repo.commit()

    return user


def remove_cookie():  # pragma: no cover
    if 'ckanext-welive-user' in pylons.session:
        del pylons.session['ckanext-welive-user']
    pylons.session.save()


class Welive_AuthenticationPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IAuthenticator)
    plugins.implements(plugins.IRoutes, inherit=True)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'welive_authentication')

    # IAuthenticator

    def identify(self):
        log.debug('identify')
        if 'Authorization' in py_req.headers:
            if py_req.headers['Authorization'].lower().startswith('bearer'):
                log.debug('Bearer Authorization')
                token = py_req.headers['Authorization']
                cc_user_id = get_user_id(token)
                if cc_user_id is not None:
                    user = get_user_by_id(cc_user_id)
                    if user is not None:
                        if user.state != 'deleted':
                            toolkit.c.user = user.name
                    else:
                        user = create_user(cc_user_id)
                        toolkit.c.user = user.name
            elif py_req.headers['Authorization'].lower().startswith('basic'):
                log.debug('Basic Authorization')
                basic_auth = py_req.headers['Authorization']
                decoded_ba = base64.b64decode(basic_auth.split(' ')[1])
                basic_user = decoded_ba.split(':')[0]
                basic_pass = decoded_ba.split(':')[1]
                if basic_user == BASIC_USER and basic_pass == BASIC_PASSWORD:
                    cc_user_id = None
                    if py_req.method == 'POST':
                        body_json = json.loads(py_req.body)
                        if 'ccUserID' in body_json:
                            cc_user_id = body_json['ccUserID']
                        else:
                            cc_user_id = ADMIN_CC_USER_ID
                        user = get_user_by_id(cc_user_id)
                    elif py_req.method == 'GET':
                        cc_user_id = py_req.queryvars.get('ccUserID')
                        if cc_user_id is None:
                            cc_user_id = ADMIN_CC_USER_ID
                        user = get_user_by_id(cc_user_id)
                    if user is not None:
                        toolkit.c.user = user.name
                    else:
                        user = create_user(cc_user_id)
                        toolkit.c.user = user.name
                        # abort(404, _('User not found'))
        else:
            username = pylons.session.get('ckanext-welive-user')
            expiration_date = pylons.session.get('ckanext-welive-expiration')
            if username and expiration_date:
                if expiration_date < datetime.datetime.now():
                    response = requests.post(
                        '%s/oauth/token' % AAC_API_URL,
                        params={'refresh_token':
                                pylons.session['ckanext-welive-refresh'],
                                'client_id': CLIENT_ID,
                                'client_secret': CLIENT_SECRET,
                                'grant_type': 'refresh_token'}).json()
                    pylons.session['ckanext-welive-token'] = '%s %s' % (response['token_type'], response['access_token'])
                    expiration_date = datetime.datetime.now() + datetime.timedelta(0, response['expires_in'])
                    pylons.session['ckanext-welive-expiration'] = expiration_date
                    pylons.session.save()
                user = model.User.by_name(username)
                if user:
                    toolkit.c.user = user.name
                    pylons.session['ckanext-welive-user'] = user.name
                    pylons.session.save()
                    for item in py_req.params:
                        if item == 'login':
                            redirect_url = '{}/{}{}'.format(CKAN_SITE_URL, h.lang(), py_req.path_url.replace(CKAN_SITE_URL, ''))
                            redirect(redirect_url)

                else:
                    remove_cookie()
            elif 'code' in request.GET:
                callback = '{}/{}/'.format(CKAN_SITE_URL, h.lang())
                if py_req.path.endswith('/dataset/new'):
                    callback += 'dataset/new'
                print '%s/oauth/token' % AAC_API_URL
                data = {'client_id': CLIENT_ID,
                        'client_secret': CLIENT_SECRET,
                        'grant_type': 'authorization_code',
                        'code': request.GET.get('code'),
                        'redirect_uri': callback
                        }
                print data
                token_json = requests.post(
                    '%s/oauth/token' % AAC_API_URL,
                    params={'client_id': CLIENT_ID,
                            'client_secret': CLIENT_SECRET,
                            'grant_type': 'authorization_code',
                            'code': request.GET.get('code'),
                            'redirect_uri': callback
                            }).json()
                if 'exception' in token_json or token_json.get('error', False):
                    remove_cookie()
                else:
                    pylons.session['ckanext-welive-accessed'] = True
                    expiration_date = datetime.datetime.now() + datetime.timedelta(0, token_json['expires_in'])
                    pylons.session['ckanext-welive-token'] = '%s %s' % (token_json['token_type'], token_json['access_token'])
                    pylons.session['ckanext-welive-refresh'] = token_json['refresh_token']
                    pylons.session['ckanext-welive-expiration'] = expiration_date
                    pylons.session.save()
                    cc_user_id = get_user_id('%s %s' % (token_json['token_type'],
                                                        token_json['access_token']))
                    if cc_user_id is not None:
                        user = get_user_by_id(cc_user_id)
                        if user is not None:
                            if user.state == 'deleted':
                                log.debug('User deleted, removing cookie...')
                                remove_cookie()
                            else:
                                toolkit.c.user = user.name
                                pylons.session['ckanext-welive-user'] = user.name
                                pylons.session.save()
                                if 'welive-ods-redirect-url' in pylons.session:
                                    redirect_url = pylons.session['welive-ods-redirect-url']
                                    del(pylons.session['welive-ods-redirect-url'])
                                    redirect(redirect_url)
                        else:
                            user = create_user(cc_user_id)
                            pylons.session['ckanext-welive-user'] = user.name
                            pylons.session.save()
                            toolkit.c.user = user.name
                            if 'welive-ods-redirect-url' in pylons.session:
                                redirect_url = pylons.session['welive-ods-redirect-url']
                                del(pylons.session['welive-ods-redirect-url'])
                                redirect(redirect_url)
                        if not pylons.session.get('ckanex-welive-alert', False):
                            log.debug('Popping message')
                            h.flash.pop_messages()
                        else:
                            del(pylons.session['ckanex-welive-alert'])
            elif request.GET.get('login', '') == 'true':
                redirect_url = '{}/{}{}'.format(CKAN_SITE_URL, h.lang(), py_req.path_url.replace(CKAN_SITE_URL, ''))
                pylons.session['welive-ods-redirect-url'] = redirect_url
                pylons.session.save()
                if py_req.path.endswith('/dataset/new'):
                    callback = '{}/{}/dataset/new'.format(
                        CKAN_SITE_URL, h.lang())
                    redirect('{}/{}/user/login?came_from={}'.format(
                        CKAN_SITE_URL, h.lang(), callback))
                else:
                    redirect('{}/{}/user/login'.format(
                        CKAN_SITE_URL, h.lang()))

    def login(self):
        log.debug('login')
        pass

    def logout(self):
        log.debug('logout')
        remove_cookie()

    def abort(self, status_code, detail, headers, comment):
        log.debug('abort')
        log.debug('Login attempt aborted: {}, {}, {}, {}'.format(
            status_code, detail, headers, comment))
        if status_code == 401:
            remove_cookie()
            h.flash('You are not allowed to create datasets.',
                    category='alert-error', ignore_duplicate=True)
            log.debug('Creating alert')
            pylons.session['ckanex-welive-alert'] = True
            if pylons.session.get('ckanext-welive-accessed'):
                del(pylons.session['ckanext-welive-accessed'])
                redirect('{}{}'.format(CKAN_SITE_URL, '/user/login'))
            else:
                redirect(CKAN_SITE_URL)
        return status_code, detail, headers, comment

    # IRoutes

    def before_map(self, map):
        with SubMapper(
                map,
                controller='ckanext.welive_authentication.controller:'
                           'WeliveAuthenticationController') as m:
            m.connect('login', '/user/login', action='login', ckan_icon='cogs')
            m.connect('register', '/user/register', action='register',
                      ckan_icon='cogs')
            m.connect('logout', '/user/_logout', action='logout',
                      ckan_icon='signout')

        return map
