"""Tests for plugin.py."""
from ckan import logic
from ckanext.welive_authentication import plugin
import ckan.tests.helpers as helpers
import ckan.model as model
import ckan
import pylons.config as config
import webtest
import mock
import json
import ConfigParser
import os
import ckan.tests.factories as factories
import unittest

ckan_config = ConfigParser.ConfigParser()
ckan_config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:authentication'
AAC_API_URL = ckan_config.get(PLUGIN_SECTION, 'aac_api_url')
CLIENT_ID = ckan_config.get(PLUGIN_SECTION, 'client_id')
CLIENT_SECRET = ckan_config.get(PLUGIN_SECTION, 'client_secret')
ADMIN_CC_USER_ID = ckan_config.get(PLUGIN_SECTION, 'admin_cc_user_id')

MAIN_SECTION = 'app:main'
CKAN_SITE_URL = ckan_config.get(MAIN_SECTION, 'ckan.site_url') + '/en/'


AUTH_TOKEN = '025a90d4-d4dd-4d90-8354-779415c0c6d8'
REFRESH_TOKEN = '618415da-dcfc- 48b6-b775- 7d0f04a71105'
FAKE_EMAIL = 'fake@welive.eu'

CC_USER_ID = 5
USER_NAME = 'John'
USER_SURNAME = 'Doe'

MOCK_RESPONSE_FULLNAME = json.dumps({"userId": "{}".format(CC_USER_ID),
                                     "name": "{}".format(USER_NAME),
                                     "surname": "{}".format(USER_SURNAME)})

MOCK_RESPONSE_NO_FULLNAME = json.dumps({"userId": "{}".format(CC_USER_ID),
                                        "name": None,
                                        "surname": None})

MOCK_POST_RESPONSE = json.dumps({'access_token': AUTH_TOKEN,
                                 'token_type': 'Bearer',
                                 'refresh_token': REFRESH_TOKEN,
                                 'expires_in': 38937,
                                 'scope':
                                 'profile.basicprofile.me, '
                                 'profile.accountprofile.me'})

EXPIRED_MOCK_POST_RESPONSE = json.dumps({'access_token': AUTH_TOKEN,
                                         'token_type': 'Bearer',
                                         'refresh_token': REFRESH_TOKEN,
                                         'expires_in': 0,
                                         'scope':
                                         'profile.basicprofile.me, '
                                         'profile.accountprofile.me'})

MOCK_RESPONSE_ACCOUNTPROFILE_WELIVE = json.dumps(
    {'accounts': {'welive': {'username': FAKE_EMAIL}}})

MOCK_RESPONSE_ACCOUNTPROFILE_GOOGLE = json.dumps(
    {'accounts': {'google': {'OIDC_CLAIM_email': FAKE_EMAIL}}})

MOCK_RESPONSE_ACCOUNTPROFILE_FACEBOOK = json.dumps(
    {'accounts': {'facebook': {'email': FAKE_EMAIL}}})

AUTH_CODE = 'fake-code'


class MockResponse(object):
    def __init__(self, content):
        self.content = content

    def json(self):
        return json.loads(self.content)


class MockUser(object):
    def __init__(self, name, state='active'):
        self.name = name
        self.state = state


class UtilsTestCase(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_authentication')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_authentication')

    def test_get_user_by_id_match(self):
        factories.User(email='user1@mail.com', name='01')
        factories.User(email='user2@mail.com', name='02')
        factories.User(email='user3@mail.com', name='03')
        user = factories.User(email='user4@mail.com', name='04')
        factories.User(email='user5@mail.com', name='05')

        recovered_user = plugin.get_user_by_id(user['name'])

        self.assertEqual(user['name'], recovered_user.name)

    def test_get_user_by_id(self):
        factories.User(email='user1@mail.com', name='01')
        factories.User(email='user2@mail.com', name='02')
        factories.User(email='user3@mail.com', name='03')
        user = factories.User(email='user4@mail.com', name='04')
        factories.User(email='user5@mail.com', name='05')

        recovered_user = plugin.get_user_by_id('4')

        self.assertEqual(user['name'], recovered_user.name)

    def test_get_user_by_id_not_found(self):
        factories.User(email='user1@mail.com', name='01')
        factories.User(email='user2@mail.com', name='02')
        factories.User(email='user3@mail.com', name='03')
        factories.User(email='user4@mail.com', name='04')
        factories.User(email='user5@mail.com', name='05')

        recovered_user = plugin.get_user_by_id('6')

        self.assertEqual(None, recovered_user)

    @mock.patch('requests.get')
    def test_get_user_email_welive(self, mock_get):
        mock_get.return_value = MockResponse(
            MOCK_RESPONSE_ACCOUNTPROFILE_WELIVE)

        email = plugin.get_user_email('Bearer {}'.format(AUTH_TOKEN))

        self.assertEqual(FAKE_EMAIL, email)
        mock_get.assert_called_with(
            '{}/accountprofile/me'.format(AAC_API_URL),
            headers={'Authorization': 'Bearer {}'.format(AUTH_TOKEN)})

    @mock.patch('requests.get')
    def test_get_user_email_google(self, mock_get):
        mock_get.return_value = MockResponse(
            MOCK_RESPONSE_ACCOUNTPROFILE_GOOGLE)

        email = plugin.get_user_email('Bearer {}'.format(AUTH_TOKEN))

        self.assertEqual(FAKE_EMAIL, email)
        mock_get.assert_called_with(
            '{}/accountprofile/me'.format(AAC_API_URL),
            headers={'Authorization': 'Bearer {}'.format(AUTH_TOKEN)})

    @mock.patch('requests.get')
    def test_get_user_email_facebook(self, mock_get):
        mock_get.return_value = MockResponse(
            MOCK_RESPONSE_ACCOUNTPROFILE_FACEBOOK)

        email = plugin.get_user_email('Bearer {}'.format(AUTH_TOKEN))

        self.assertEqual(FAKE_EMAIL, email)
        mock_get.assert_called_with(
            '{}/accountprofile/me'.format(AAC_API_URL),
            headers={'Authorization': 'Bearer {}'.format(AUTH_TOKEN)})

    @mock.patch('requests.get')
    def test_get_user_id(self, mock_get):
        mock_get.return_value = MockResponse(MOCK_RESPONSE_FULLNAME)

        user_id = plugin.get_user_id('Bearer {}'.format(AUTH_TOKEN))

        self.assertEqual(CC_USER_ID, int(user_id))
        mock_get.assert_called_with(
            '{}/basicprofile/me'.format(AAC_API_URL),
            headers={'Authorization': 'Bearer {}'.format(AUTH_TOKEN)})

    def test_create_user(self):
        user = plugin.create_user(5)

        params = {'id': user.id}
        recovered_user = helpers.call_action('user_show', {}, **params)

        self.assertEqual(user.id, recovered_user['id'])
        self.assertEqual('WeLive User 05', recovered_user['fullname'])
        self.assertEqual('05', recovered_user['name'])


class IAuthenticatorTestCase(unittest.TestCase):

    def mock_post(self, url, params):
        if params['grant_type'] == 'authorization_code':
            return MockResponse(EXPIRED_MOCK_POST_RESPONSE)
        return MockResponse(MOCK_POST_RESPONSE)

    def setUp(self):
        model.repo.init_db()
        app = ckan.config.middleware.make_app(config['global_conf'], **config)
        self.app = webtest.TestApp(app)
        ckan.plugins.load('welive_authentication')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_authentication')

    def _get_app(self):
        return self.app

    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    @mock.patch('ckanext.welive_authentication.plugin.get_user_id')
    def test_identity_bearer(self, mock_get_user_id, mock_get_user_by_id):

        mock_get_user_id.return_value = '1'
        mock_get_user_by_id.return_value = MockUser('01')

        user = factories.User(email='user1@mail.com', name='01')

        app = self._get_app()
        app.post('/api/action/package_create',
                 params=json.dumps({'name': 'test-dataset'}),
                 headers={'Authorization':
                          'Bearer {}'.format(AUTH_TOKEN)})

        params = {'id': 'test-dataset'}
        dataset = helpers.call_action('package_show', {}, **params)

        self.assertEqual('test-dataset', dataset['name'])
        self.assertEqual(user['id'], dataset['creator_user_id'])
        mock_get_user_id.assert_called_with('Bearer {}'.format(AUTH_TOKEN))
        mock_get_user_by_id.assert_called_with('1')

    @mock.patch('ckanext.welive_authentication.plugin.create_user')
    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    @mock.patch('ckanext.welive_authentication.plugin.get_user_id')
    def test_identity_bearer_new_user(self, mock_get_user_id,
                                      mock_get_user_by_id, mock_create_user):

        mock_get_user_id.return_value = '1'
        mock_get_user_by_id.return_value = None
        mock_create_user.return_value = MockUser('01')

        app = self._get_app()
        app.post('/api/action/package_create',
                 params=json.dumps({'name': 'test-dataset'}),
                 headers={'Authorization':
                          'Bearer {}'.format(AUTH_TOKEN)})

        params = {'id': 'test-dataset'}
        dataset = helpers.call_action('package_show', {}, **params)

        self.assertEqual('test-dataset', dataset['name'])
        mock_get_user_id.assert_called_with('Bearer {}'.format(AUTH_TOKEN))
        mock_get_user_by_id.assert_called_with('1')
        mock_create_user.assert_called_with('1')

    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    def test_identity_basic_post(self, mock_get_user_by_id):
        mock_get_user_by_id.return_value = MockUser('01')

        user = factories.User(email='user1@mail.com', name='01')

        app = self._get_app()
        app.post_json(
            '/api/action/package_create',
            dict(ccUserID=1, name='test-dataset'),
            headers={'Authorization':
                     'BASIC d2VsaXZlLXRlc3Q6d2VsaXZlLXRlc3Q='})

        params = {'id': 'test-dataset'}
        dataset = helpers.call_action('package_show', {}, **params)

        self.assertEqual(user['id'], dataset['creator_user_id'])
        mock_get_user_by_id.assert_called_with(1)

    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    def test_identity_basic_post_no_ccuserid(self, mock_get_user_by_id):
        mock_get_user_by_id.return_value = MockUser('05')

        user = factories.User(email='admin@mail.com', name='05')

        app = self._get_app()
        app.post_json(
            '/api/action/package_create',
            dict(name='test-dataset'),
            headers={'Authorization':
                     'BASIC d2VsaXZlLXRlc3Q6d2VsaXZlLXRlc3Q='})

        params = {'id': 'test-dataset'}
        dataset = helpers.call_action('package_show', {}, **params)

        self.assertEqual(user['id'], dataset['creator_user_id'])
        mock_get_user_by_id.assert_called_with(ADMIN_CC_USER_ID)

    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    def test_identity_basic_get_no_ccuserid(self, mock_get_user_by_id):
        mock_get_user_by_id.return_value = MockUser('05')

        user = factories.User(email='admin@mail.com', name='05', sysadmin=True)
        organization = factories.Organization(name='test-organization')

        params = {'name': 'test-dataset', 'creator_user_id': user['id'],
                  'private': True, 'owner_org': organization['name']}
        helpers.call_action('package_create', {'user': user['name']}, **params)

        app = self._get_app()
        response = app.get('/api/action/package_show',
                           dict(id='test-dataset'),
                           headers={'Authorization':
                                    'BASIC d2VsaXZlLXRlc3Q6d2VsaXZlLXRlc3Q='})
        dataset = response.json['result']

        self.assertEqual(user['id'], dataset['creator_user_id'])
        self.assertEqual('test-dataset', dataset['name'])
        mock_get_user_by_id.assert_called_with(ADMIN_CC_USER_ID)

    @mock.patch('ckanext.welive_authentication.plugin.get_user_id')
    @mock.patch('ckanext.welive_authentication.plugin.get_user_by_id')
    @mock.patch('requests.post')
    def test_identity_web_existing_user(self, mock_post, mock_get_user_by_id,
                                        get_user_id):
        mock_get_user_by_id.return_value = MockUser('05')
        get_user_id.return_value = 5
        mock_post.return_value = MockResponse(MOCK_POST_RESPONSE)

        factories.User(email='admin@mail.com', name='05')

        app = self._get_app()

        response = app.get('/?code={}'.format(AUTH_CODE))
        session = response.session

        assert('ckanext-welive-expiration' in session)
        assert(session['ckanext-welive-token'] == 'Bearer {}'.format(
            AUTH_TOKEN))
        assert(session['ckanext-welive-refresh'] == REFRESH_TOKEN)
        assert(session['ckanext-welive-user'] == '0{}'.format(CC_USER_ID))

        mock_post.assert_called_with(
            '{}/oauth/token'.format(AAC_API_URL),
            params={'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET,
                    'grant_type': 'authorization_code',
                    'code': AUTH_CODE,
                    'redirect_uri': CKAN_SITE_URL
                    }
        )
